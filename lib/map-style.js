export default [
  {
    stylers: [
      { visibility: 'off' },
    ],
  },
  {
    featureType: 'landscape.natural.landcover',
    stylers: [
      { color: '#E0E0E0' },
      { visibility: 'on' },
    ],
  },
  {
    featureType: 'administrative.country',
    stylers: [
      { visibility: 'on' },
      { lightness: 100 },
      { weight: 2 },
    ],
  },
  {
    featureType: 'administrative.province',
    stylers: [
      { visibility: 'on' },
      { lightness: 100 },
      { weight: 1.2 },
    ],
  },
  {
    featureType: 'administrative.province',
    elementType: 'labels',
    stylers: [
      { visibility: 'off' },
    ],
  },
  {
    featureType: 'administrative.country',
    elementType: 'labels',
    stylers: [
      { visibility: 'off' },
    ],
  },
  {
    featureType: 'water',
    stylers: [
      { visibility: 'on' },
      { lightness: 100 },
    ],
  },
];
