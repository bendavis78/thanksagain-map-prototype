import $ from 'jquery';

const BULLSEYE_URL = 'http://ws.bullseyelocations.com/RestSearch.svc/';

export default class BullseyeClient {
  constructor(baseParams) {
    this.baseParams = baseParams;
  }

  call(method, params = {}) {
    const data = Object.assign(this.baseParams, params);
    return new Promise((resolve, reject) => {
      $.ajax(BULLSEYE_URL + method, {
        data,
        success(result) {
          resolve(result);
        },
        error(error) {
          reject(error);
        },
      });
    });
  }

  search(params) {
    return this.call('DoSearch2', params);
  }

  getCatSum() {
    return this.call('GetCatSum', {});
  }
}
