import $ from 'jquery';
import EventEmitter from 'eventemitter3';
import GoogleMaps from 'google-maps';

export default class MapMarker extends EventEmitter {
  constructor(opts) {
    super();
    this.id = opts.id;
    this.data = opts.data;
    this.merchants = opts.merchantsApi;
    this.gmap = opts.map;
    this.tooltip = opts.tooltip || this.data.Name;
    this._marker = new GoogleMaps.Marker({
      map: opts.map,
      icon: opts.icon,
      position: { lat: this.data.Latitude, lng: this.data.Longitude },
    });
    this._marker.addListener('click', () => {
      this.emit('tapped', this);
    });
    this._marker.addListener('mouseover', this.showTooltip.bind(this));
    this._marker.addListener('mouseout', this.hideTooltip.bind(this));
    this.gmap.addListener('bounds_changed', () => {
      $('#taMapTooltip').hide();
    });
  }
  remove() {
    this._marker.setMap(null);
  }
  get point() {
    if (!this._marker || !this.gmap) {
      return null;
    }
    const bounds = this.gmap.getBounds();
    const topRight = this.gmap.getProjection().fromLatLngToPoint(bounds.getNorthEast());
    const bottomLeft = this.gmap.getProjection().fromLatLngToPoint(bounds.getSouthWest());
    const scale = Math.pow(2, this.gmap.getZoom());
    const p = this.gmap.getProjection().fromLatLngToPoint(this._marker.getPosition());
    return new GoogleMaps.Point((p.x - bottomLeft.x) * scale, (p.y - topRight.y) * scale);
  }
  showTooltip() {
    const div = this.gmap.getDiv();
    const $div = $(div);
    let $el = $('#taMapTooltip');
    if (!$el.length) {
      $el = $div.append('<div id="taMapTooltip"><span></span></div>').find('#taMapTooltip');
    }
    $el.find('span').text(this.tooltip);
    const midpoint = div.offsetWidth / 2;
    let direction = 'right';
    const css = {
      position: 'absolute',
      top: div.offsetTop + this.point.y,
      maxWidth: midpoint,
    };
    if (this.point.x > midpoint) {
      direction = 'left';
      css.right = div.offsetWidth - this.point.x;
      css.left = '';
    } else {
      css.left = div.offsetLeft + this.point.x;
      css.right = '';
    }
    $el.css(css);
    $el.toggleClass('right', direction === 'right');
    $el.toggleClass('left', direction === 'left');
    $el.show();
  }
  hideTooltip() {
    $('#taMapTooltip').hide();
  }
  showInfoWindow() {
    /*
    this._infoWindow.open(this.gmap, this._marker);
    return this.merchants.getCatSum(this.data.PostalCode).then(cats => {
      const $ul = $(`#infoWindow${this.id} ul`);
      for (const cat of cats) {
        $(`<li class="${cat[0]}" title="${cat[1]}">${cat[0]}</li>`).appendTo($ul);
      }
    });
    */
  }
}
