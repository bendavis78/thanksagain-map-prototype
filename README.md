# Local development

Your development environment should be configured to develop with Node.js. Refer
to the installation instructions relevant to your system.

* [Node.js website](https://nodejs.org/en/)

Also, make sure jspm is installed globally using the following command:

    $ npm install jspm -g

Then, within this directory, install the dependencies and start the development
server:

    $ jspm install
    $ npm start

To view documentation:

    $ npm run docs

To build the distribution package:

    $ npm run build
